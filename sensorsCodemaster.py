# requires to find machine id
from uuid import getnode as get_mac
# Import necessary libraries
import time
# spidev used for serial data communication (ADC converter)
import spidev
import sys
import serial
import requests
import json
firebase_url = 'https://test-2434b.firebaseio.com/'
import RPi.GPIO as GPIO
# pigpio used for DHT22 sensor
import pigpio
pi = pigpio.pi()
import DHT22
sensor = DHT22.sensor(pi, 4)

# variable store mac id
mac = get_mac()

# use this to figure out the mac id if it is a new pi then comment it out
# print(mac)

# every pi has a unique mac id and it is given an id for our understanding.
mac_id_map = {128480595478853: 1}
# This checks if the raspberrypi mac id is added to the map
print("---------------------------------------\n")
if mac in mac_id_map:
    UNIQUE_ID = mac_id_map.get(mac)
    print("Unique Raspberry Pi ID: " + str(UNIQUE_ID) + "\n")
else:
    print("please add mac id to the map in script!\n")
print("---------------------------------------\n")

# Open SPI Bus
spi = spidev.SpiDev()
spi.open(0, 0)

# creating Logfile to read and Fluentd file for data logging to database
Logfile = time.strftime("%d_%m_%Y_airQualityData.txt")
f = open(Logfile, "w")
f.write("")
f.close()

# reading the data channel on the ADC converter to gather data from
# various sensors


def ReadChannel(channel):
    if channel > 7 or channel < 0:
        return -1
    adc = spi.xfer2([1, 8 + channel << 4, 0])
    data = ((adc[1] & 3) << 8) + adc[2]
    return data

# convert data to voltage level


def ConvertVolts(data, places):
    volts = (data * 5) / float(1023)
    volts = round(volts, places)
    return volts

# Functions for measuring the PM, measures two different signals, one for PM size 2.5-10 um, and the other for PM<10um
# The way this sensing works is we record how long the sensor is tripped in a given amount of time. This is the OnSense functions for each signal.
# When the given amount fo time is up, we average it over the 15 seconds
# (Average function) and plug it into an equation from a spec sheet Andy
# and I found.

# detect for signal from PM sized 2.5 - 10 um


def OnSense2510(channel):
    global falling, total2510, t02510
    # if the sensor gets tripped (rising edge), start the timer and set the
    # flag (falling) to 0
    if falling > 1:
        t02510 = time.time()
        falling = 0
    # if the sensor gets tripped again (falling edge), record the time, find the difference in time and add it to the total
    # also set the flag (falling) to 2
    elif falling < 1:
        l2510 = time.time() - t02510
        falling = 2
        total2510 = total2510 + l2510

# detect for signal from PM sized 1 - 10 um, same as above but different signal


def OnSense10(channel):
    global falling2, total10, t010
    if falling2 > 1:
        t010 = time.time()
        falling2 = 0
    elif falling2 < 1:
        l10 = time.time() - t010
        falling2 = 2
        total10 = total10 + l10

# calculating the average time the sensor is tripped over 15 seconds


def Average(t2510, t10):
    global total2510, total10
    # averages, Avg 10 needed to be converted from milliseconds for some reason
    Avg2510 = t2510 / 15 * 100
    Avg10 = t10 / 15000 * 100
    # particle count, use polynomial equation from spec sheet curve: units are
    # number of particles per 0.01 cubic feet
    PM10 = 1.1 * ((t2510 + t10 / 1000) / 15 * 100)**3 - 3.8 * ((t2510 +
                                                                t10 / 1000) / 15 * 100)**2 + 520 * ((t2510 + t10 / 1000) / 15 * 100)
    PM25 = 1.1 * Avg10**3 - 3.8 * Avg10**2 + 520 * Avg10
    # create an array to reference the values from
    mean = [Avg2510, Avg10, PM10, PM25]
    # reset total times to 0 for next set of readings
    # the totals are used to count how long the sensor has been triggered over
    # the 15 seconds, and for each new set of 15 seconds, they have to be
    # reset to zero
    total2510 = 0
    total10 = 0
    return mean


def Analyze():
    # PM sensor data call
    avg = Average(total2510, total10)
    # opening the file used to save data to
    f = open(Logfile, "a")
    # creating the timestamp variable
    timestamp = time.strftime("%d/%m/%Y %H:%M:%S")
    print("Reading after %d seconds" % seconds)
    # setting up table heading
    print("Timestamp\t\tCO(ppb)\t\tNOx Ozone(ppb)\t\tNox(ppb)\t\tPM1\t\tPM2\t\tHumidity\t\tTemperature(C*)\n")
    # printing the timestamp to the terminal
    print("%s\t" % (timestamp), end="")
    f.write(str(timestamp).rstrip('\n') + "\t\t\t")
    # looping through each sensor to gather the raw data and then print it to
    # the screen, and save it to the txt file
    for i in range(len(sensorsArray)):
        data = ReadChannel(sensorsArray[i])
        volts = ConvertVolts(data, 2)
        if i == 0:
            data = (data / 1024.0) * 500
            data = float("{0:.3f}".format(data))
            cO_Val = data
            print(str(data) + "\t\t", end="")
            f.write(str(data).rstrip('\n') + "\t\t")
            #print("CO Volts: %s" % str(volts))
        elif i == 1:
            data = (data / 1024.0) * 20
            data = float("{0:.3f}".format(data))
            nox_Ozone_Val = data
            print(str(data) + "\t\t\t", end="")
            #print("NOX Ozone Volts: %s" % str(volts))
            f.write(str(data).rstrip('\n') + "\t\t\t")
        elif i == 2:
            data = (data / 1024.0) * 20
            data = float("{0:.3f}".format(data))
            nox_Val = data
            print(str(data) + "\t\t\t", end="")
            #print("NOX Volts: %s" % str(volts))
            f.write(str(data).rstrip('\n') + "\t\t\t")

    # writing pm data to the text file
    # writing avg[2] to the txt file which corresponds with the PM 10 reading
    data = float("{0:.3f}".format(avg[2]))
    pm1 = data
    print(str(data) + "\t\t", end="")
    f.write(str(data).rstrip('\n') + "\t\t")
    # writing avg[3] to the txt file which corresponds with the PM 2.5 reading
    data = float("{0:.3f}".format(avg[3]))
    print(str(data) + "\t\t", end="")
    pm2 = data
    f.write(str(data).rstrip('\n') + "\t\t")

    # pigpio temperature in Celcius
    sensor.trigger()
    # humidity sensor details
    data = float('{:3.2f}'.format(sensor.humidity() / 1.))
    humid = data
    print(str(data) + "\t\t\t", end="")
    f.write(str(data).rstrip('\n') + "\t\t\t")
    # temperature sensor details
    data = float('{:3.2f}'.format(sensor.temperature() / 1.))
    temper = data
    print(str(data) + "\n", end="")
    f.write(str(data).rstrip('\n') + "\n")
    # adding data to Google Firebase
    data = {'id': UNIQUE_ID,
            'Timestamp': timestamp,
            'CO': cO_Val,
            'NOX Ozone': nox_Ozone_Val,
            'NOX': nox_Val,
            'PM Value 1': pm1,
            'PM Value 2': pm2,
            'humidity': humid,
            'temperature': temper}
    result = requests.post(
        firebase_url + '/' + str(mac) + '/aqsensor.json', data=json.dumps(data))

    print("--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------\n")
    f.write("--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------\n")
    f.close()


# sensor channels
#cOSensor = 0
#noxO3 = 1
#noxSensor = 2
#temperature = 3
sensorsArray = [0, 1, 2, 3]

# set up GPIO channels (2) as inputs, also initialize other variables
total2510 = 0
total10 = 0
GPIO.setmode(GPIO.BOARD)
falling = 2
falling2 = 2
pm2510 = 40
pm10 = 38
GPIO.setup(pm2510, GPIO.IN)
GPIO.setup(pm10, GPIO.IN)
GPIO.add_event_detect(pm2510, GPIO.BOTH, callback=OnSense2510)
GPIO.add_event_detect(pm10, GPIO.BOTH, callback=OnSense10)

# sleep timer
delay = 3
# seconds count
seconds = 0
f = open(Logfile, "a")
f.write("Timestamp\t\t\t\tCO(ppm)\t\tNOx Ozone(ppm)\t\tNox(ppm)\t\tPM1\t\tPM2\t\tHumidity\t\tTemperature(C*)\n")
f.close()

# main function starts here!
while True:
    try:
        time.sleep(delay)
        seconds = seconds + delay
        Analyze()
    except KeyboardInterrupt:
        break

print(" Bye!")
sys.exit()
