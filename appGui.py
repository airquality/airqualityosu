from tkinter import *

root = Tk()

root.state('zoomed')
root.wm_title("Air Quality Concept")

topFrame = Frame(root)
topFrame.pack()

bottomFrame = Frame(root)
bottomFrame.pack(side=BOTTOM)

button1 = Button(topFrame, text="Button1", fg="red")
button2 = Button(topFrame, text="Button2", fg="blue")
button3 = Button(bottomFrame, text="Button3", fg="green")
button4 = Button(bottomFrame, text="Button4", fg="purple")

button1.pack(side=LEFT)
button2.pack(side=LEFT)
button3.pack(side=BOTTOM)
button4.pack(side=BOTTOM)

# make sure program keeps running
root.mainloop()

