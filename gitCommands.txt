*******************things you need to do only once*******************

//git is a system that helps you maintain code
//Why is this important? One example is If you write a new part of code and if 
//it is not working, git will help you to go back to the previous working version!

//install git
sudo apt-get install git

//initializing folder
git init

//adding the location you need to save the code
git remote add origin https://airqualityosu@bitbucket.org/airquality/airqualityosu.git

*******************things you do to update the code*******************

//before pushing new code to the repository the first you need to do is check if
//the code repository has newer code and if so update
git pull origin master

//adding all the files PS: make sure to delete all the data files before adding
git add .

//adding the changes you did to the file
git commit -m "Enter message to understand the changes you did - add your name"

//pushing the code to the website
git push origin master


